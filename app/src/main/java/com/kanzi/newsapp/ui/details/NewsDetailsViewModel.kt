package com.kanzi.newsapp.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.kanzi.domain.model.Article
import com.kanzi.newsapp.ui.utils.ext.Event
import com.kanzi.newsapp.ui.utils.ext.getValue
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

private const val ARG_ARTICLE = "article"

@HiltViewModel
class NewsDetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val article by savedStateHandle.getValue<Article>(ARG_ARTICLE)

    private val _state = MutableLiveData(ViewState(article))
    val state: LiveData<ViewState> get() = _state

    fun tappedReadOnline() {
        onReduceEvent(Action.TappedReadOnline)
    }

    private fun onReduceEvent(event: Action) {
        when (event) {
            Action.TappedReadOnline -> {
                _state.value = _state.value!!.copy(readOnline = Event(article.url))
            }
        }
    }

    sealed class Action {
        object TappedReadOnline : Action()
    }

    data class ViewState(
        val article: Article,
        val readOnline: Event<String>? = null
    )
}
