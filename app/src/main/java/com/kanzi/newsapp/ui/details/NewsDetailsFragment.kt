package com.kanzi.newsapp.ui.details

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import coil.load
import com.kanzi.newsapp.R
import com.kanzi.newsapp.databinding.FragmentDetailsBinding
import com.kanzi.newsapp.ui.utils.DateUtils
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsDetailsFragment : Fragment(R.layout.fragment_details) {

    private val viewModel: NewsDetailsViewModel by viewModels()

    private lateinit var binding: FragmentDetailsBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailsBinding.bind(view)
        binding.detailsToolbarBack.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.detailsReadFull.setOnClickListener {
            viewModel.tappedReadOnline()
        }
        observeState()
    }

    private fun observeState() {
        viewModel.state.observe(
            viewLifecycleOwner
        ) { state ->
            binding.detailsImage.load(state.article.urlToImage)
            binding.detailsTitle.text = state.article.title
            binding.detailsAuthor.text = state.article.author
            binding.detailsDescription.text = state.article.description
            binding.detailsDate.text = DateUtils.parseDateTime(state.article.publishedAt)

            state.readOnline?.getContentIfNotHandled()?.let {
                CustomTabsIntent
                    .Builder()
                    .build()
                    .launchUrl(requireContext(), Uri.parse(it))
            }
        }
    }
}
