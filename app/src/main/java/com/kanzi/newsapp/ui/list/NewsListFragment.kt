package com.kanzi.newsapp.ui.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.kanzi.newsapp.R
import com.kanzi.newsapp.databinding.FragmentListBinding
import com.kanzi.newsapp.ui.utils.adapter.DelegateAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsListFragment : Fragment(R.layout.fragment_list) {

    private val viewModel: NewsListViewModel by viewModels()

    private lateinit var binding: FragmentListBinding

    private val adapter by lazy { DelegateAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentListBinding.bind(view)
        binding.newsList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = this@NewsListFragment.adapter
        }
        binding.newsRefreshLayout.setOnRefreshListener {
            viewModel.refreshNews()
        }
        observeState()
    }

    private fun observeState() {
        viewModel.state.observe(
            viewLifecycleOwner
        ) { state ->
            binding.newsRefreshLayout.isRefreshing = state.isLoading
            state.articles.map {
                NewsDisplayableItem(it) {
                    viewModel.tappedArticle(it)
                }
            }.also {
                adapter.updateItems(it)
            }
            state.showArticle?.getContentIfNotHandled()?.let {
                val action = NewsListFragmentDirections.actionListToDetails(it)
                findNavController().navigate(action)
            }
            state.showError?.getContentIfNotHandled()?.let {
                Snackbar
                    .make(binding.root, getString(R.string.api_error), Snackbar.LENGTH_LONG)
                    .show()
            }
        }
    }
}
