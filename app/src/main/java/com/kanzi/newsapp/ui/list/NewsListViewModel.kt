package com.kanzi.newsapp.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kanzi.domain.model.Article
import com.kanzi.domain.usecase.GetNewsUseCase
import com.kanzi.newsapp.ui.utils.ext.Event
import com.kanzi.newsapp.ui.utils.ext.SimpleEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

private const val COUNTRY_CODE = "LT"

@HiltViewModel
class NewsListViewModel @Inject constructor(
    private val getNewsUseCase: GetNewsUseCase
) : ViewModel() {

    private val _state = MutableLiveData(ViewState())
    val state: LiveData<ViewState> get() = _state

    private val disposable = CompositeDisposable()

    init {
        refreshNews()
    }

    fun refreshNews() {
        onReduceEvent(Action.ReceivedGetNews)
    }

    fun tappedArticle(article: Article) {
        onReduceEvent(Action.TappedArticle(article))
    }

    private fun getNews() {
        getNewsUseCase(COUNTRY_CODE)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { response ->
                    onReduceEvent(Action.ReceivedNews(response))
                },
                onError = {
                    onReduceEvent(Action.ReceivedError)
                }
            )
            .addTo(disposable)
    }

    private fun onReduceEvent(event: Action) {
        when (event) {
            Action.ReceivedGetNews -> {
                _state.value = _state.value!!.copy(isLoading = true)
                getNews()
            }
            is Action.ReceivedNews -> {
                _state.value = _state.value!!.copy(
                    isLoading = false,
                    articles = event.articles
                )
            }
            is Action.TappedArticle -> {
                _state.value = _state.value!!.copy(showArticle = Event(event.article))
            }
            Action.ReceivedError -> {
                _state.value = _state.value!!.copy(isLoading = false, showError = SimpleEvent())
            }
        }
    }

    sealed class Action {
        object ReceivedGetNews : Action()
        data class ReceivedNews(val articles: List<Article>) : Action()
        data class TappedArticle(val article: Article) : Action()
        object ReceivedError : Action()
    }

    data class ViewState(
        val isLoading: Boolean = false,
        val articles: List<Article> = emptyList(),
        val showArticle: Event<Article>? = null,
        val showError: SimpleEvent? = null
    )

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}
