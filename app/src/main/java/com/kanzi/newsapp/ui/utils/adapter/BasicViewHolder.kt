package com.kanzi.newsapp.ui.utils.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class BasicViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
