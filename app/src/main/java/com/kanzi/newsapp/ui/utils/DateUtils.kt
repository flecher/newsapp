package com.kanzi.newsapp.ui.utils

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

object DateUtils {

    fun parseDateTime(stringDate: String): String {
        return OffsetDateTime
            .parse(stringDate)
            .format(DateTimeFormatter.ofPattern("yyyy-MMMM-dd hh:mm"))
    }
}
