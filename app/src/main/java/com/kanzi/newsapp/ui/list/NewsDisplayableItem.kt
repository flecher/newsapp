package com.kanzi.newsapp.ui.list

import android.view.View
import coil.load
import com.kanzi.domain.model.Article
import com.kanzi.newsapp.R
import com.kanzi.newsapp.databinding.ItemNewsBinding
import com.kanzi.newsapp.ui.utils.DateUtils
import com.kanzi.newsapp.ui.utils.adapter.DisplayableItem

class NewsDisplayableItem(
    private val article: Article,
    val onClick: () -> Unit
) : DisplayableItem {
    override val layoutId: Int = R.layout.item_news

    override fun isSameItem(other: DisplayableItem): Boolean {
        return other is NewsDisplayableItem
    }

    override fun isSameContent(other: DisplayableItem): Boolean {
        return other is NewsDisplayableItem && article == other.article
    }

    override fun bind(view: View) {
        super.bind(view)
        ItemNewsBinding.bind(view).apply {
            newsImage.load(article.urlToImage)
            newsTitle.text = article.title
            newsDate.text = DateUtils.parseDateTime(article.publishedAt)
        }
        view.setOnClickListener {
            onClick()
        }
    }
}
