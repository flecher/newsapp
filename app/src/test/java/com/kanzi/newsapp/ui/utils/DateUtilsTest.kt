package com.kanzi.newsapp.ui.utils

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class DateUtilsTest {

    private lateinit var utils: DateUtils

    @BeforeEach
    fun setUp() {
        utils = DateUtils
    }

    @Nested
    inner class DateTimeParsing {

        private val dateTimeString = "2021-07-27T05:31:00Z"
        private val correctParsedDate = "2021-liepos-27 05:31"

        @Test
        fun `date is parsed correctly`() {
            assertEquals(correctParsedDate, utils.parseDateTime(dateTimeString))
        }
    }
}
