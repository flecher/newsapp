package com.kanzi.newsapp.ui.list

import com.kanzi.domain.model.Article
import com.kanzi.domain.usecase.GetNewsUseCase
import com.kanzi.newsapp.utils.InstantExecutorExtension
import com.kanzi.newsapp.utils.RxSchedulerExtension
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(value = [RxSchedulerExtension::class, InstantExecutorExtension::class])
internal class NewsListViewModelTest {

    private val getNewsUseCase = mockk<GetNewsUseCase>()

    private val countryCode = "LT"

    private lateinit var viewModel: NewsListViewModel

    @Nested
    inner class GetNewsSuccess {

        private val response = listOf(
            mockk<Article>()
        )

        @BeforeEach
        fun setUp() {
            every { getNewsUseCase(countryCode) } returns Single.just(response)
            viewModel = NewsListViewModel(getNewsUseCase)
        }

        @Test
        fun `is not loading`() {
            assertEquals(false, viewModel.state.value!!.isLoading)
        }

        @Test
        fun `article list is one item`() {
            assertEquals(1, viewModel.state.value!!.articles.size)
        }
    }

    @Nested
    inner class GetNewsFailure {

        @BeforeEach
        fun setUp() {
            every { getNewsUseCase(countryCode) } returns Single.error(Throwable("error"))
            viewModel = NewsListViewModel(getNewsUseCase)
        }

        @Test
        fun `is not loading`() {
            assertEquals(false, viewModel.state.value!!.isLoading)
        }

        @Test
        fun `error is shown`() {
            assertNotNull(viewModel.state.value!!.showError)
        }
    }

    @Nested
    inner class TappedArticle {

        private val response = listOf(
            mockk<Article>(),
            mockk<Article>(),
            mockk<Article>()
        )

        @BeforeEach
        fun setUp() {
            every { getNewsUseCase(countryCode) } returns Single.just(response)
            viewModel = NewsListViewModel(getNewsUseCase)
            viewModel.tappedArticle(response.first())
        }

        @Test
        fun `show article is not null`() {
            assertNotNull(viewModel.state.value!!.showArticle)
        }
    }
}
