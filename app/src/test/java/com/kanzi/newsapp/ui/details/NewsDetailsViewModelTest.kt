package com.kanzi.newsapp.ui.details

import androidx.lifecycle.SavedStateHandle
import com.kanzi.domain.model.Article
import com.kanzi.newsapp.utils.InstantExecutorExtension
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(value = [InstantExecutorExtension::class])
internal class NewsDetailsViewModelTest {

    private val savedStateHandle = mockk<SavedStateHandle>()

    private lateinit var viewModel: NewsDetailsViewModel

    private val article = Article(
        "test author",
        "title nice",
        "very long description",
        "https://www.delfi.lt",
        "https://www.image.com",
        "2021-07-26T12:29:45Z",
        "some kind of content"
    )

    @BeforeEach
    fun setUp() {
        every { savedStateHandle.get<Any>(any()) } returns article
        viewModel = NewsDetailsViewModel(savedStateHandle)
    }

    @Test
    fun `article is present`() {
        assertEquals(article, viewModel.state.value!!.article)
    }

    @Nested
    inner class TappedReadMore {

        @BeforeEach
        fun setUp() {
            viewModel.tappedReadOnline()
        }

        @Test
        fun `url is correctly set`() {
            assertEquals(article.url, viewModel.state.value!!.readOnline?.peekContent())
        }
    }
}
