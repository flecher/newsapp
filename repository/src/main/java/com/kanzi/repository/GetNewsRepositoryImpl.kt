package com.kanzi.repository

import com.kanzi.api.RemoteApi
import com.kanzi.domain.model.Article
import com.kanzi.domain.repository.GetNewsRepository
import com.kanzi.repository.ext.toArticle
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetNewsRepositoryImpl @Inject constructor(
    private val api: RemoteApi
) : GetNewsRepository {

    override fun getTopHeadlines(countryCode: String): Single<List<Article>> {
        return api.getTopHeadlines(countryCode).map { it.toArticle() }
    }
}
