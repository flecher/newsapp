package com.kanzi.repository.ext

import com.kanzi.api.models.ArticleResponse
import com.kanzi.domain.model.Article

fun ArticleResponse.toArticle(): List<Article> {
    return articles.map {
        Article(
            it.author,
            it.title,
            it.description,
            it.url,
            it.urlToImage,
            it.publishedAt,
            it.content
        )
    }
}
