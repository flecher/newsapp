package com.kanzi.repository

import com.kanzi.domain.repository.GetNewsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {

    @Provides
    fun provideGetNewsRepository(getNewsRepositoryImpl: GetNewsRepositoryImpl): GetNewsRepository {
        return getNewsRepositoryImpl
    }
}
