package com.kanzi.repository

import com.kanzi.api.RemoteApi
import com.kanzi.api.models.ArticleResponse
import com.kanzi.domain.model.Article
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class GetNewsRepositoryImplTest {

    private lateinit var newsRepositoryImpl: GetNewsRepositoryImpl
    private val api = mockk<RemoteApi>()
    private val countryCode = "LT"

    @BeforeEach
    fun setUp() {
        newsRepositoryImpl = GetNewsRepositoryImpl(api)
    }

    @Nested
    inner class Success {

        private lateinit var observer: TestObserver<List<Article>>

        private val response = ArticleResponse(
            "ok",
            1,
            listOf(
                com.kanzi.api.models.Article(
                    mockk(),
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                ),
                com.kanzi.api.models.Article(
                    mockk(),
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                ),
                com.kanzi.api.models.Article(
                    mockk(),
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                )
            )
        )

        @BeforeEach
        fun setUp() {
            every { api.getTopHeadlines(any()) } returns Single.just(response)
            observer = newsRepositoryImpl.getTopHeadlines(countryCode).test()
        }

        @Test
        fun `verify api is called`() {
            verify { api.getTopHeadlines(countryCode) }
        }

        @Test
        fun `received response`() {
            val result = observer.values().last()
            assertEquals(3, result.size)
        }
    }

    @Nested
    inner class Failure {

        private lateinit var observer: TestObserver<List<Article>>

        @BeforeEach
        fun setUp() {
            every { api.getTopHeadlines(any()) } returns Single.error(Throwable("error"))
            observer = newsRepositoryImpl.getTopHeadlines(countryCode).test()
        }

        @Test
        fun `verify api is called`() {
            verify { api.getTopHeadlines(countryCode) }
        }

        @Test
        fun `received error`() {
            observer.assertFailure(Throwable::class.java)
        }
    }
}
