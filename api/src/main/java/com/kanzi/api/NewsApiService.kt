package com.kanzi.api

import com.kanzi.api.models.ArticleResponse
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class NewsApiService @Inject constructor(
    private val api: NewsApi
) : RemoteApi {
    override fun getTopHeadlines(countryCode: String): Single<ArticleResponse> {
        return api.getTopHeadlines(countryCode)
    }
}
