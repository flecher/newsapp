package com.kanzi.api

import com.kanzi.api.models.ArticleResponse
import io.reactivex.rxjava3.core.Single

interface RemoteApi {
    fun getTopHeadlines(countryCode: String): Single<ArticleResponse>
}
