package com.kanzi.api

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface ApiModule {

    @Binds
    fun bindRemoteApi(service: NewsApiService): RemoteApi
}
