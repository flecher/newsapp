package com.kanzi.api

import com.kanzi.api.models.ArticleResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET("top-headlines")
    fun getTopHeadlines(
        @Query("country") countryCode: String
    ): Single<ArticleResponse>
}
