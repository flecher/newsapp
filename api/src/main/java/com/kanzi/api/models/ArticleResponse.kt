package com.kanzi.api.models

data class ArticleResponse(
    val status: String,
    val totalResults: Int,
    val articles: List<Article>
)
