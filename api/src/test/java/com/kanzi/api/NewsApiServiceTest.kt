package com.kanzi.api

import com.kanzi.api.models.ArticleResponse
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class NewsApiServiceTest {

    private lateinit var service: NewsApiService
    private val newsApi = mockk<NewsApi>()

    private val countryCode = "LT"

    @BeforeEach
    fun setUp() {
        service = NewsApiService(newsApi)
    }

    @Nested
    inner class Success {

        private lateinit var observer: TestObserver<ArticleResponse>

        @BeforeEach
        fun setUp() {
            every { newsApi.getTopHeadlines(any()) } returns Single.just(mockk())
            observer = service.getTopHeadlines(countryCode).test()
        }

        @Test
        fun `api method is called`() {
            verify { newsApi.getTopHeadlines(countryCode) }
        }

        @Test
        fun `received response`() {
            val result = observer.values().last()
            assert(result is ArticleResponse)
        }
    }

    @Nested
    inner class Failure {

        private lateinit var observer: TestObserver<ArticleResponse>

        @BeforeEach
        fun setUp() {
            every { newsApi.getTopHeadlines(any()) } returns Single.error(Throwable("error"))
            observer = service.getTopHeadlines(countryCode).test()
        }

        @Test
        fun `api method is called`() {
            verify { newsApi.getTopHeadlines(countryCode) }
        }

        @Test
        fun `received error`() {
            observer.assertFailure(Throwable::class.java)
        }
    }
}
