package com.kanzi.domain.usecase

import com.kanzi.domain.model.Article
import com.kanzi.domain.repository.GetNewsRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class GetNewsUseCaseTest {

    private lateinit var useCase: GetNewsUseCase
    private val newsRepository = mockk<GetNewsRepository>()
    private val countryCode = "LT"

    @BeforeEach
    fun setUp() {
        useCase = GetNewsUseCase(newsRepository)
    }

    @Nested
    inner class Success {

        private lateinit var observer: TestObserver<List<Article>>

        @BeforeEach
        fun setUp() {
            every { newsRepository.getTopHeadlines(any()) } returns Single.just(
                listOf(
                    mockk(),
                    mockk()
                )
            )
            observer = useCase(countryCode).test()
        }

        @Test
        fun `repository method is called`() {
            verify { newsRepository.getTopHeadlines(countryCode) }
        }

        @Test
        fun `received items`() {
            val result = observer.values().last()
            assertEquals(2, result.size)
        }
    }

    @Nested
    inner class Failure {

        private lateinit var observer: TestObserver<List<Article>>

        @BeforeEach
        fun setUp() {
            every { newsRepository.getTopHeadlines(any()) } returns Single.error(Throwable("error"))
            observer = useCase(countryCode).test()
        }

        @Test
        fun `repository method is called`() {
            verify { newsRepository.getTopHeadlines(countryCode) }
        }

        @Test
        fun `received error`() {
            observer.assertFailure(Throwable::class.java)
        }
    }
}
