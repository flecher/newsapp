package com.kanzi.domain.usecase

import com.kanzi.domain.model.Article
import com.kanzi.domain.repository.GetNewsRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetNewsUseCase @Inject constructor(
    private val getNewsRepository: GetNewsRepository
) {
    operator fun invoke(countryCode: String): Single<List<Article>> {
        return getNewsRepository.getTopHeadlines(countryCode)
    }
}
