package com.kanzi.domain.repository

import com.kanzi.domain.model.Article
import io.reactivex.rxjava3.core.Single

interface GetNewsRepository {
    fun getTopHeadlines(countryCode: String): Single<List<Article>>
}
